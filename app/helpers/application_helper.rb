module ApplicationHelper
  

  # Get list of supported languages
  def get_languages_list
    [["English","en"],["Español","es"]]#,["Português","pt"]]
  end

  def change_default_locale(lan)
    config.I18n.default_locale = lan
  end
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
  	base_title = "PassList"
  	if page_title.empty?
  		base_title
  	else
  		"#{page_title} | #{base_title}"
  	end
  end

  # Get the placeholder for a given field in a form
  def get_form_ph(e,k,expl = "")
  	if e.any?
  		e.messages[k][0] if !e.messages[k].nil?
  	else
  		return k.to_s.humanize if expl.empty?
  		expl
  	end
  end

  # Get the class for a given field in a form. Used to turn it red when erros occur
  def get_form_group_cl(e,k)
  	if e.any?
  		"has-error" if e.messages[k] != nil
  	end
  end

  # Get the title to be shown before the sitelist partial for a given user
  def get_sites_title(count)
  	params[:count] = count
  	return t(:no_sites_found) if count == 0
  	t(:configured_sites_list)
  end

  def app_version
    "1.0"
  end
end
