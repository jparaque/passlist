class ApplicationMailer < ActionMailer::Base
	require "mail"
	address = Mail::Address.new "noreply@passlist.org"
	address.display_name = "PassList"
 	default from: address.format
  	layout 'mailer'
end
