class SessionsController < ApplicationController
	protect_from_forgery with: :exception
	include SessionsHelper
	def new
		if logged_in?
			redirect_to current_user
		end
		@login_error = ""
	end

	def create
		user = User.find_by(email: get_session_params[:email].downcase)
		if user && user.authenticate(params[:session][:password])
			log_in user
			remember user if params[:session][:remember_me]
			redirect_to user
		else
			@login_error = "Wrong email/password combination"
			render 'new'
		end
	end

	def destroy
		log_out if logged_in?
    	redirect_to root_url
	end
	
	def get_session_params
		params.require(:session).permit(:email,:password)
	end

end
