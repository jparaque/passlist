class UsersController < ApplicationController
	include SessionsHelper
	include ApplicationHelper
	require 'digest/sha1'

	before_action :correct_user, only: [:show,:edit,:update]

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			I18n.locale = user_params[:locale].to_sym
			request_activation_email
			flash[:info] = t(".info")
			log_in @user
			redirect_to @user
		else 
			render "new"
		end
	end

	def show
		@user = User.find(params[:id])
		respond_to do |format|
			format.html
			format.js
		end
	end

	def getpass
		@user = current_user
		params[:pass_hash] = Digest::SHA1.hexdigest(params[:pass].to_s)
		params.delete(:pass)
		render "show"
	end

	def edit
		@user = correct_user
	end

	def update
		@user = correct_user
		if @user.update_attributes(user_params)
			I18n.locale = user_params[:locale].to_sym
			flash[:success] = t(".success")
			redirect_to @user
		else
			render "edit"
		end
	end

	def request_activation_email(redirect=false)
		user = @user ? @user : nil
		if params[:redirect] || redirect
			user = current_user
			user.create_activation_digest 
			user.save
		end
		UserMailer.account_activation(user).deliver_now
		if params[:redirect] || redirect
			flash[:info] = t(".flash_send")
			redirect_to user
		end
	end

	private 

	def user_params
		params.require(:user).permit(:name, :email, :password, :password_confirmation,:locale)
	end

	def correct_user
		@user = User.find_by(id: params[:id])
		redirect_to root_path unless @user == current_user && @user
		@user
	end

end
