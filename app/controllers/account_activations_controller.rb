class AccountActivationsController < ApplicationController
	include SessionsHelper

	def edit
		user = User.find_by(email: params[:email])
		if user && user.authenticated?(:activation,params[:id])
			user.update_attribute(:activated, true)
			user.update_attribute(:activated_at, Time.zone.now)
			login user if !logged_in?
			flash[:success] = t(".activation_success")
			redirect_to user
		else
			flash[:danger] = t(".activation_error")
			if logged_in?
				redirect_to user 
			else
				redirect_to root_path
			end
		end
	end
end
