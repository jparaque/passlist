class ApplicationController < ActionController::Base
	include SessionsHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale

  def set_locale
    if current_user
      I18n.locale = current_user.locale || I18n.default_locale
    elsif cookies[:locale]
      I18n.locale = cookies[:locale].to_sym
    end
  end
end
