class SitesController < ApplicationController
	include SessionsHelper
	before_action :logged_in_user, only: [:refresh_salt,:destroy,:update]
	before_action :correct_user, only: [:destroy,:update,:update]


	def new
		if !current_user.activated
			flash[:warning] = t(".activate_flash_html", url: activation_email_url(id: current_user.id, redirect: true))
			redirect_to user_path(current_user)
		end
		@site = Site.new
	end

	def create
		@site = Site.new(site_params)
		if @site.save
			@site.update(salt: @site.gen_salt)
			#flash[:sucess] = t("controllers.sites.create.sucess", name: @site.site)
			redirect_to user_path(site_params[:user_id])
		else
			render "new"
		end
	end

	def refresh_salt
		@site = Site.find(params[:data][:id])
		@site.refresh_salt
		flash[:success] = t(".flash_success", name: @site.site)
		redirect_to User.find(@site.user_id), pass_hash: params[:data][:pass]
	end

	def edit
		@site = Site.find(params[:id])
	end

	def update
		@site = Site.find(params[:id])
		if @site.update_attributes(site_params)
			flash[:success] = t(".success",name: @site.site)
			redirect_to user_path(User.find(site_params[:user_id]))
		else
			render "edit"
		end
	end

	def destroy
		@site = Site.find(params[:id])
    	@site.destroy
    	#flash[:success] = "Site #{@site.site} has been deleted."
    	redirect_to current_user
	end

	def site_params
		params.require(:site).permit(:id,:site,:nchar,:user_id,:allow_special,:method)
	end

	def correct_user
		@site = current_user.sites.find_by(id: params[:id])
		redirect_to current_user if @site.nil?
	end
end
