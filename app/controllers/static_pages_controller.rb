class StaticPagesController < ApplicationController
	include SessionsHelper
	include ApplicationHelper

  def home
  	logger.debug "-----> Logged in?: #{logged_in?}"
  	if logged_in?
  		redirect_to current_user
  	end
  end

	def change_locale
		if params.include? :locale 
			cookies.permanent[:locale] = params[:locale]
		end
		redirect_to root_path
	end

	def home
		if logged_in?
			redirect_to current_user
		end
	end
end
