require "valid_email"
require "bcrypt"

class User < ActiveRecord::Base
	attr_accessor :remember_token, :activation_token, :reset_token

	validates :name, presence: true
	validates :email, presence: true, confirmation: true,
		email: true, uniqueness: {case_sensitive: false}
	validates :password, length: {minimum: 8}, allow_blank: true
	has_many :sites
	before_save :downcase_email
	before_create :create_activation_digest
	has_secure_password

	def self.new_token
		SecureRandom.urlsafe_base64
	end

	def User.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
  	end

	def remember
		self.remember_token = User.new_token
		update_attribute(:remember_digest, User.digest(remember_token))
	end

	def forget
		update_attribute(:remember_digest,nil)
	end

	# Returns true if the given token matches the digest.
  	def authenticated?(digest,token)
  		#valid = BCrypt::Password.new(remember_digest).is_password?(remember_token)
  		#logger.debug "Valid token: #{valid}"
  		digest = self.send("#{digest}_digest")
    	result = digest ? BCrypt::Password.new(digest).is_password?(token) : false
  	end

  	def create_activation_digest
  		if !self.activated
  			self.activation_token = User.new_token
  			self.activation_digest = User.digest(activation_token)
  		end
  	end

  	def create_reset_digest
  		self.reset_token = User.new_token
  		update_attribute(:reset_digest, User.digest(reset_token))
  		update_attribute(:reset_sent_at, Time.zone.now)
  	end

  	 def send_password_reset_email
    	UserMailer.password_reset(self).deliver_now
  	end

  	def password_reset_expired?
  		reset_sent_at < 2.hours.ago
  	end

  	private

  	def downcase_email
  		self.email = email.downcase
  	end
end
