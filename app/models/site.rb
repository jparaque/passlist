class Site < ActiveRecord::Base
	require 'digest/sha1'
	include BCrypt

	belongs_to :user 
	validates :nchar, numericality: {greater_than: 8, less_than_or_equal_to: 25, message: I18n.t(:site_error_nchar_value)}
	validates :method, inclusion: { in: %w(sha1 sha1_64 special_chars)}
	validates :allow_special, length: {minimum: 0}, allow_blank: true


	def generate_pass(pass)
		return "" if pass.nil?
		self.update(salt: gen_salt) if self.salt.nil?
		self.update(method: "sha1") if self.method.nil?
		case self.method
		when "sha1"
			return Digest::SHA1.hexdigest(self.salt+"__"+pass.to_s)[0..nchar-1]
		when "sha1_64"
			return Digest::SHA1.base64digest(self.salt+"__"+pass.to_s)[0..nchar-1]
		when "special_chars"
			pass = Digest::SHA1.base64digest(self.salt+"__"+pass.to_s)[0..nchar-1]
			rng = Random.new (/\d+/.match(pass)).to_s.to_i
			len = (0.5*self.nchar).to_i
			vals = allow_special.split("").sample(len,random: rng)
			puts vals
			puts pass
			used = []
			vals.each do |x|
				pos = 0
				loop do
					pos = rng.rand(0..(nchar-1))
					break if !used.include? pos
				end
				used<<pos
				puts "Val: #{x}, pos: #{pos}"
				pass[pos] = x
			end
			return pass
		end
	end

	def gen_salt
		num = rand(10000).to_s
		seed = self.created_at.to_s+"__"+num
		BCrypt::Password.create(seed)
	end

	def refresh_salt
		self.update(salt: gen_salt)
	end
end
