# PassList #

[PassList](https://powerful-caverns-6387.herokuapp.com) is a web application written in rails which generates different passwords for each site in your site list based on a master password that must be entered every time the list of password is needed.

### In a nutshell ###

* Sign up.
* Start adding sites and configure them to generate passwords.
* Get the passwords associated with your master password.

### Important! ###

* No password is stored.
* All passwords are calculated when you enter your master password.

### License ###

PassList is released under [MIT License](http://opensource.org/licenses/MIT) following the rails licensing.