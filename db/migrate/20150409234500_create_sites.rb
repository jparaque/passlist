class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :site
      t.integer :nchar

      t.timestamps null: false
    end
  end
end
