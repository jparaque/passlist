class AddAllowSpecialToSites < ActiveRecord::Migration
  def change
    add_column :sites, :allow_special, :string, default: "-_=()<>" 
  end
end
