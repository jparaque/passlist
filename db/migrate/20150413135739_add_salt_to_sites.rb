class AddSaltToSites < ActiveRecord::Migration
  def change
    add_column :sites, :salt, :string
  end
end
